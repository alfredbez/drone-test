<?php
class Calculator{
  /**
   * gibt die Summe von $values aus
   * @param  array $values
   * @return float         Summe von $values
   */
  public function sum($values)
  {
    $summe = 0;
    foreach($values as $value){
      $summe += $value;
    }
    return $summe;
  }
}
?>