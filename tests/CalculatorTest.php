<?php

class CalculatorTest extends PHPUnit_Framework_TestCase
{
  public function testCanSum()
  {
    $c = new Calculator();
    $this->assertEquals(10, $c->sum([1,2,3,4]));
  }
}
?>